########################################
# PowerPC-specific definitions

CFLAGS-$(CONFIG_POWERPC_64) += -m64 -mabi=elfv2

powerpc-mcpu-$(CONFIG_POWERPC_ISA_3_0) := power9

# Note that -mcmodel=medany is used so that Xen can be mapped
# into the upper half _or_ the lower half of the address space.
# -mcmodel=medlow would force Xen into the lower half.

CFLAGS += -mcpu=$(powerpc-mcpu-y) -mstrict-align -mcmodel=medium
CFLAGS += -I$(BASEDIR)/include
