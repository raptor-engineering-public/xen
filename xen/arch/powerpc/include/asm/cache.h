#ifndef __ARCH_POWERPC_CACHE_H
#define __ARCH_POWERPC_CACHE_H

/* L1 cache line size */
#define L1_CACHE_SHIFT	(CONFIG_POWERPC_L1_CACHE_SHIFT)
#define L1_CACHE_BYTES	(1 << L1_CACHE_SHIFT)

#define __read_mostly __section(".data.read_mostly")

#ifndef __ASSEMBLY__

void cache_writeback(const void *addr, unsigned int size);

#endif

#endif
